package at.campus02.dbp2.flights;

import at.campus02.dbp2.flights.domain.Customer;
import at.campus02.dbp2.flights.domain.Flight;
import at.campus02.dbp2.flights.domain.Seat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FlightsDaoImpl implements FlightsDao {

    private EntityManager manager;
    private EntityManagerFactory factory;

    public FlightsDaoImpl(EntityManagerFactory factory) {
        this.factory = factory;
        manager = factory.createEntityManager();
    }

    @Override
    public boolean create(Flight flight) {
        if (flight == null /*|| flight.getFlightNumber() == null*/) {
            return false;
        } else if(manager.find(Flight.class, flight.getFlightNumber()) == null) {

            manager.getTransaction().begin();
            //for each seat from flight x >> set flight for seat
                for (Seat s : flight.getSeats()) {
                    s.setFlight(flight);
                }
            manager.persist(flight);
            manager.getTransaction().commit();
            if (manager.contains(manager.find(Flight.class, flight.getFlightNumber()))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Flight readFlight(String flightNumber) {
        if (flightNumber == null) {
            return null;
        }
        return manager.find(Flight.class, flightNumber);
    }

    @Override
    public Flight update(Flight flight) {
        if (flight == null) {
            return null;
        } else {
            manager.getTransaction().begin();
            for (Seat s : flight.getSeats()) {
                s.setFlight(flight);
            }
            manager.getTransaction().commit();
            return manager.find(Flight.class, flight.getFlightNumber());
        }
    }

    @Override
    public boolean delete(Flight flight) {
        if (flight == null || flight.getFlightNumber() == null || manager.find(Flight.class, flight.getFlightNumber()) == null) {
            return false;
        } else {
            manager.getTransaction().begin();
            manager.remove(flight);
            manager.getTransaction().commit();
            return true;
        }
    }

    @Override
    public boolean create(Customer customer) {
        if (customer == null || customer.getEmail() == null) {
            return false;
        } else if(manager.find(Customer.class,customer.getEmail()) == null) {
            manager.getTransaction().begin();
            manager.persist(customer);
            manager.getTransaction().commit();
                if (manager.contains(manager.find(Customer.class, customer.getEmail()))) {
                    return true;
            }
        }
        return false;
    }

    @Override
    public Customer readCustomer(String email) {
        if (email == null) {
            return null;
        }
        return manager.find(Customer.class, email);
    }

    @Override
    public Customer update(Customer customer) {
        if (customer == null) {
            return null;
        } else {
            manager.getTransaction().begin();
            manager.getTransaction().commit();
            return manager.find(Customer.class, customer.getEmail());
        }
    }

    @Override
    public boolean delete(Customer customer) {
        if (customer == null || customer.getEmail() == null || manager.find(Customer.class,customer.getEmail()) == null) {
            return false;
        } else {
            List<Seat> results = manager.createQuery("SELECT s FROM Seat s WHERE s.customer =:customer", Seat.class).setParameter("customer", customer).getResultList();
            manager.getTransaction().begin();
                for (Seat s : results) {
                    s.setCustomer(null);
                }
            manager.remove(customer);
            manager.getTransaction().commit();
            return true;
        }
    }

    @Override
    public List<Flight> findFlightsByDestination(String destination) {
        if (destination == null) {
            return manager.createQuery("SELECT f FROM Flight f").getResultList();
        } else {
            return manager.createQuery("SELECT f FROM Flight f WHERE lower(f.destination) = :destination", Flight.class).setParameter("destination", destination.toLowerCase()).getResultList();
        }
    }

    @Override
    public List<Customer> findCustomersBy(String lastname, String firstname) {
        if (firstname == null && lastname != null) {
            return manager.createQuery("SELECT c FROM Customer c WHERE lower(c.lastname) =:lastname", Customer.class)
                        .setParameter("lastname", lastname.toLowerCase()).getResultList();
        }
        else if (firstname != null && lastname == null) {
            return manager.createQuery("SELECT c FROM Customer c WHERE lower(c.firstname) =:firstname", Customer.class)
                    .setParameter( "firstname", firstname.toLowerCase()).getResultList();
        }
        else if (firstname == null && lastname == null) {
            return manager.createQuery("SELECT c FROM Customer c", Customer.class).getResultList();
        }
        else {
            return manager.createQuery("SELECT c FROM Customer c WHERE lower(c.lastname) =:lastname AND lower(c.firstname) =:firstname", Customer.class)
                    .setParameter("lastname", lastname.toLowerCase()).setParameter("firstname", firstname.toLowerCase()).getResultList();
        }
    }

    @Override
    public List<Seat> findAvailableSeatsTo(String destination) {
        if (destination == null) {
            return manager.createQuery("SELECT s FROM Seat s WHERE s.customer is null", Seat.class).getResultList();
        }
        return manager.createQuery("SELECT s FROM Seat s WHERE lower(s.flight.destination) =:destination and s.customer is null", Seat.class)
                .setParameter("destination", destination.toLowerCase()).getResultList();

    }

    @Override
    public List<Seat> findSeatsBookedBy(Customer customer) {
       return manager.createQuery("SELECT s FROM Seat s WHERE s.customer =:customer", Seat.class).setParameter("customer", customer).getResultList();
        //return manager.createNamedQuery("Seat.findSeatsBookedBy").setParameter("customer", customer).getResultList();
        //zum vergleich: code von der kollegin
        //return manager.createNamedQuery("Flight.findFlightsByDestination").setParameter("destination", destination2).getResultList();
    }

    @Override
    public boolean reserve(Seat seat, Customer customer) {
        if  (customer == null
                || seat == null
                || seat.getId() == null
                || manager.find(Customer.class, customer.getEmail()) == null
                || manager.find(Seat.class, seat.getId()).getCustomer() != null) {
            return false;
        }
        else {
            manager.getTransaction().begin();
            seat.setCustomer(customer);
            manager.getTransaction().commit();
                if (seat.getCustomer().equals(customer)) {
                    return true;
                }
        }
            return false;
    }

    @Override
    public boolean cancel(Seat seat, Customer customer) {
        if  (customer == null
                || seat == null
                || seat.getId() == null
                || manager.find(Customer.class, customer.getEmail()) == null
                || manager.find(Seat.class, seat.getId()).getCustomer() == null) {
            return false;
        }
        else {
            manager.getTransaction().begin();
            seat.setCustomer(null);
            manager.getTransaction().commit();
            if (seat.getCustomer() == null) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void close() {
        if (this.manager != null && this.manager.isOpen()) {
            this.manager.close();
        }
    }
}
