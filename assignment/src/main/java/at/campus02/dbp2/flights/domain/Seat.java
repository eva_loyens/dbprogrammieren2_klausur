package at.campus02.dbp2.flights.domain;

import javax.persistence.*;


//named query funktioniert nicht: "Annotations are not allowed here"
//@NamedQuery(name = "Seat.findSeatsBookedBy", query = "SELECT s FROM Seat s WHERE s.customer= :customer");

@Entity
public class Seat {

    @Id @GeneratedValue
    private Integer id;
    private String seatNumber;
    private SeatType seatType;
    @ManyToOne (cascade = CascadeType.PERSIST)
    private Flight flight;
    //Mehrere Sitze kann 1 Customer sein > many to one
    @ManyToOne (cascade = CascadeType.PERSIST)
    private Customer customer;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Seat seat = (Seat) o;

        return id != null ? id.equals(seat.id) : seat.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public Integer getId() {
        return id;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public SeatType getType() {
        return seatType;
    }

    public void setType(SeatType type) {
        this.seatType = type;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
