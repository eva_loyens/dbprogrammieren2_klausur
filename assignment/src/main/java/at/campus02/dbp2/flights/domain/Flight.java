package at.campus02.dbp2.flights.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Flight {
    @Id
    private String flightNumber;
    private String destination;
    // 1 Flug mehrere Sitze > oneToMany
    @OneToMany (mappedBy = "flight", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Seat> seats = new ArrayList<Seat>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flight flight = (Flight) o;

        return flightNumber != null ? flightNumber.equals(flight.flightNumber) : flight.flightNumber == null;
    }

    @Override
    public int hashCode() {
        return flightNumber != null ? flightNumber.hashCode() : 0;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<Seat> getSeats() {
        return seats;
    }
}
