package at.campus02.dbp2.flights;

import at.campus02.dbp2.flights.domain.Customer;
import at.campus02.dbp2.flights.domain.Flight;
import at.campus02.dbp2.flights.domain.Seat;
import at.campus02.dbp2.flights.domain.SeatType;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class IntegrationTest extends BaseSpecification {

    @Test
    public void integrationTest() {

        // given
        // "raw" data
        Customer customer1 = prepareCustomer("last1", "first1", "mail1");
        Customer customer2 = prepareCustomer("last2", "first2", "mail2");
        Customer customer3 = prepareCustomer("last3", "first3", "mail3");
        Customer customer4 = prepareCustomer("last4", "first4", "mail4");
        Customer customer5 = prepareCustomer("last5", "first5", "mail5");

        Flight flight1 = prepareFlight(flightNr1, graz);
        Flight flight2 = prepareFlight(flightNr2, graz);
        Flight flight3 = prepareFlight(flightNr3, wien);

        Seat seat1 = prepareSeat("12A", SeatType.WINDOW);
        Seat seat2 = prepareSeat("12B", SeatType.MIDDLE);
        Seat seat3 = prepareSeat("12C", SeatType.AISLE);
        Seat seat4 = prepareSeat("12D", SeatType.AISLE);
        Seat seat5 = prepareSeat("12E", SeatType.MIDDLE);
        Seat seat6 = prepareSeat("12F", SeatType.WINDOW);
        Seat seat7 = prepareSeat("17A", SeatType.WINDOW);

        // assign seats to flights
        flight1.getSeats().add(seat1);
        flight1.getSeats().add(seat2);
        flight2.getSeats().add(seat3);
        flight2.getSeats().add(seat4);
        flight2.getSeats().add(seat5);
        flight3.getSeats().add(seat6);
        flight3.getSeats().add(seat7);

        // when
        flightsDao.create(customer1);
        flightsDao.create(customer2);
        flightsDao.create(customer3);
        flightsDao.create(customer4);
        flightsDao.create(customer5);

        flightsDao.create(flight1);
        flightsDao.create(flight2);
        flightsDao.create(flight3);

        flightsDao.reserve(seat1, customer1);
        flightsDao.reserve(seat2, customer2);
        flightsDao.reserve(seat4, customer3);
        flightsDao.reserve(seat5, customer3);

        // then
        assertThat(flightsDao.findAvailableSeatsTo(graz), hasItems(seat3));
        assertThat(flightsDao.findAvailableSeatsTo(wien), hasItems(seat6, seat7));
        assertThat(flightsDao.findFlightsByDestination(graz), hasItems(flight1, flight2));
        assertThat(flightsDao.findFlightsByDestination(wien), hasItems(flight3));
        assertThat(flightsDao.findSeatsBookedBy(customer2), hasItems(seat2));
        assertThat(flightsDao.findSeatsBookedBy(customer3), hasItems(seat4, seat5));

        // and when ... data is removed
        flightsDao.delete(flight2);
        flightsDao.delete(customer4);

        // then
        assertThat(flightsDao.findAvailableSeatsTo(graz).isEmpty(), is(true));
        assertThat(flightsDao.findAvailableSeatsTo(wien), hasItems(seat6, seat7)); // customer4 deleted, flight7 also
        assertThat(flightsDao.findFlightsByDestination(graz), hasItems(flight1));
        assertThat(flightsDao.findFlightsByDestination(wien), hasItems(flight3));
        assertThat(flightsDao.findSeatsBookedBy(customer2), hasItems(seat2));
        assertThat(flightsDao.findSeatsBookedBy(customer3).isEmpty(), is(true));
        assertThat(flightsDao.findSeatsBookedBy(customer4).isEmpty(), is(true));
        assertThat(flightsDao.findCustomersBy("last4", "first4").isEmpty(), is(true));

        // also check database, there should be
        // - 2 flights ("OS 172" to Graz, "LN 4332" to Wien)
        // - 4 customers
        // - 4 seats (2 reserved, 2 free)
    }

}
