package at.campus02.dbp2.flights;

import at.campus02.dbp2.flights.domain.Customer;
import at.campus02.dbp2.flights.domain.Flight;
import at.campus02.dbp2.flights.domain.Seat;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TrickyFlightsSpecification extends BaseSpecification {
    @Test
    public void findCustomersByNeitherLastnameNorFirstnameReturnsAllCustomers() {
        // given
        setUpTestData();

        // when
        List<Customer> customers = flightsDao.findCustomersBy(null, null);

        // then
        assertThat(customers.size(), is(3));
        assertThat(customers.contains(customer1), is(true));
        assertThat(customers.contains(customer2), is(true));
        assertThat(customers.contains(customer3), is(true));
    }
    @Test
    public void findFlightsByDestinationSearchesCaseInsensitive() {
        // given
        setUpTestData();

        // when
        List<Flight> flights = flightsDao.findFlightsByDestination("graz");

        // then
        assertThat(flights.size(), is(2));
        assertThat(flights.contains(flight1), is(true));
        assertThat(flights.contains(flight2), is(true));
    }
    @Test
    public void findCustomerSearchesCaseInsensitive() {
        // given
        setUpTestData();

        // when
        List<Customer> customers = flightsDao.findCustomersBy("huber", "hansi");

        // then
        assertThat(customers.size(), is(1));
        assertThat(customers.contains(customer1), is(true));
    }
    @Test
    public void findAvailableSeatsInNullReturnsAllAvailableSeats() {
        // given
        setUpTestData();

        // when
        List<Seat> seats = flightsDao.findAvailableSeatsTo(null);

        // then
        assertThat(seats.isEmpty(), is(false));
    }
    @Test
    public void findAvailableSeatsSearchesCaseInsensitive() {
        // given
        setUpTestData();

        // when
        List<Seat> seats = flightsDao.findAvailableSeatsTo("graz");

        // then
        assertThat(seats.size(), is(availableSeatsToGraz.size()));
        assertThat(seats.containsAll(availableSeatsToGraz), is(true));

        // and when
        seats = flightsDao.findAvailableSeatsTo("wien");

        // then
        assertThat(seats.size(), is(availableSeatsToWien.size()));
        assertThat(seats.containsAll(availableSeatsToWien), is(true));
    }

}
