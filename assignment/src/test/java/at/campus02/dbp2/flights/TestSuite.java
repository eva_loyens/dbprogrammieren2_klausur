package at.campus02.dbp2.flights;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Suite.SuiteClasses( {
        CrudSpecification.class,
        FlightsSpecification.class,
        TrickyCrudSpecification.class,
        TrickyFlightsSpecification.class,
        IntegrationTest.class
} )
public class TestSuite {
}
